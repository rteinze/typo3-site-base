<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'BD Bulma Theme',
	'description' => 'TYPO3 theme to build sites with bulma.io',
	'category' => 'templates',
	'author' => 'Rene Teinze',
	'author_email' => 'r.teinze@buero-digitale.de',
	'author_company' => 'buero digitale GmbH',
	'state' => 'stable',
	'clearCacheOnLoad' => true,
	'version' => '1.0.3',
	'constraints' => [
		'depends' => [
			'typo3' => '8.7.0-12.9.99',
			'php' => '7.4.0-8.2.99',
		],
		'conflicts' => [],
	],
	'uploadfolder' => 0,
	'createDirs' => '',
];
