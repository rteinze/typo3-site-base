// Quelle: https://css-tricks.com/styling-based-on-scroll-position/
// geaendert wegen Abwaertskompatibilitaet
// init
;(function () {
// The debounce function receives our function as a parameter
    var debounce = function (fn) {

        // This holds the requestAnimationFrame reference, so we can cancel it if we wish
        var frame;

        // The debounce function returns a new function that can receive a variable number of arguments
        return function (/*...params*/) {

            // If the frame variable has been defined, clear it now, and queue for next frame
            if (frame) {
                cancelAnimationFrame(frame);
            }

            // Queue our function call for the next frame
            frame = requestAnimationFrame(function() {

                // Call our function and pass any params we received
                fn(/*...params*/);
            });

        }
    };


// Reads out the scroll position and stores it in the data attribute
// so we can use it in our stylesheets
    var storeScroll = function () {
        document.documentElement.dataset.scrollPosition = window.scrollY;

        hooks();
    }


    var hooks = function () {
        ;(function () {
            var scrollPos = +document.documentElement.dataset.scrollPosition;

            jQuery('.show-on-scroll')[scrollPos < 800 ? 'addClass' : 'removeClass']('hidden');
        })()
    }


    var replacePosters = function () {
        jQuery('video').each(function (index, elVideo) {
            var
                elSource = $(this).children('source')[0]
                , src = elSource.src
                , filename = src.substring(0, src.lastIndexOf('.'));

            console.log(filename,elVideo, elSource);

            // replace poster
            if (!elVideo.poster) {
                elVideo.poster = filename + '.jpg';
            }

        })
    }


    // replace poster from video
    replacePosters();

    // activate floating menu
    jQuery('#floating-menu').on('mouseover', function () {
        jQuery('#floating-menu').addClass('is-active');
    })
    jQuery('#floating-menu').on('mouseout', function () {
        jQuery('#floating-menu').removeClass('is-active');
    })
    jQuery('#floating-menu').on('click', function () {
        jQuery('#floating-menu').removeClass('is-active');
    })

// Listen for new scroll events, here we debounce our `storeScroll` function
    document.addEventListener('scroll', debounce(storeScroll), { passive: true });

    // replace section index in menus
    jQuery('a[href*="[self]#"]').each(function (index,elm) {
        var
            sectionIndex = elm.href.substr(elm.href.indexOf('[self]#')+6)

        elm.href = sectionIndex;
    })

// Update scroll position for first time
    storeScroll();


})();
