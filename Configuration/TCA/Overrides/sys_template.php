<?php
defined('TYPO3') or die('Access denied.');

call_user_func(function()
{
	/**
	 * Extension key
	 */
	$extensionKey = 'bd_bulma_theme';

	/**
	 * Default TypoScript
	 */
	TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
		$extensionKey,
		'Configuration/TypoScript',
		'BD Bulma Theme'
	);
});