<?php
defined('TYPO3') or die('Access denied.');

// RTE configuration
#$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets'] = [
#	'default' => 'EXT:bd_bulma_theme/Configuration/RTE/Default.yaml',
#	'minimal' => 'EXT:rte_ckeditor/Configuration/RTE/Minimal.yaml',
#	'full' => 'EXT:rte_ckeditor/Configuration/RTE/Full.yaml',
#	];

// zweite CKeditor Config
//$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['test'] = 'fileadmin/CkEditor/Test.yaml';

// backend css
#$GLOBALS['TBE_STYLES']['skins'][$_EXTKEY]['stylesheetDirectories'][] = 'EXT:bd_bulma_theme/Resources/Public/Css/Backend/';

// static pageTSConfig
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:bd_bulma_theme/Configuration/TsConfig/Default.typoscript">');
